//
//  ViewController.swift
//  OneWithCustomCoreDataStack
//
//  Created by Asha Chakrapani on 6/22/18.
//  Copyright © 2018 Asha Chakrapani. All rights reserved.
//

import UIKit
import CoreData

//faults
//perform vs performAndWait
//connection pool - single write and multiple reads


class MainViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("Main view controller - viewDidLoad")
        let _ = CoreDataStack.sharedInstance.mainContext
        self.insertTasks()
        self.printTasks()
    }
    
    
    //MARK: Private API
    private let appDelegate = UIApplication.shared.delegate as! AppDelegate
    private lazy var coreDataController = CoreDataController()
    private var selectedTaskMoc: TaskMOC?
    private var tasks: [TaskMOC]?
    private var taskCount: Int?
    
    private func insertTasks() {
        self.taskCount = self.coreDataController.getTaskCount(onContext: CoreDataStack.sharedInstance.mainContext)
        
        if let _taskCount = self.taskCount {
            let task: Task = Task(taskTitle: "Task \(_taskCount)", taskDescription: "Task \(_taskCount) description", taskType: TaskType.tenantCanvas, status: TaskStatus.invalid, notes: "test notes  \(_taskCount)", tenantCanvas: TenantCanvas(tenantId: _taskCount * 20, accessLevel: 2, locations: [Location(locationId: _taskCount, locationName: "location \(_taskCount)", isActive: true)]))
//            let task: Task = Task(taskTitle: "Task \(_taskCount)", taskDescription: "Task \(_taskCount) description", taskType: TaskType.tenantCanvas, tenantId: _taskCount * 20, status: TaskStatus.invalid, notes: "test notes  \(_taskCount)", locations: [Location(locationId: _taskCount, locationName: "location \(_taskCount)", isActive: true)])

            let _ = TaskMOC.insertInto(context: CoreDataStack.sharedInstance.mainContext, task: task)
            do {
                try CoreDataStack.sharedInstance.mainContext.save()
            } catch {
                print ("Error saving seed data \(error)")
            }
        }
    }

//    private func updateARandomLocation() {
//        if let _taskCount = self.taskCount {
//            let location = Location(locationId: _taskCount, locationName: "location \(_taskCount)", isActive: true)
//            self.coreDataController.updateLocation(forTaskId: Int32(_taskCount), location: location, onContext: CoreDataStack.sharedInstance.mainContext)
//        }
//    }
    
    private func printTasks() {
        if let tasks = self.coreDataController.getAllTasks(onContext: CoreDataStack.sharedInstance.mainContext) {
            for task in tasks {
                print(task.tenantCanvas?.locations?.allObjects as? [LocationMOC] ?? [])
            }
        }
    }
}





