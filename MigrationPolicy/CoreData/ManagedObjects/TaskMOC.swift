//
//  TaskMOC.swift
//  MigrationPolicy
//
//  Created by Asha Chakrapani on 6/22/18.
//  Copyright © 2018 Asha Chakrapani. All rights reserved.
//

import Foundation
import CoreData

@objc(TaskMOC)
public class TaskMOC: NSManagedObject {
    
    static func insertInto(context: NSManagedObjectContext, task: Task) -> TaskMOC? {
        var objectMOC : TaskMOC? = nil
        if let entity = NSEntityDescription.entity(forEntityName: "Task", in: context) {
            objectMOC = TaskMOC(entity: entity, insertInto: context)
            objectMOC?.taskTitle = task.taskTitle
            objectMOC?.taskDescription = task.taskDescription
            let taskStatus = task.status?.rawValue ?? TaskStatus.invalid.rawValue
            let taskType = task.taskType?.rawValue ?? TaskType.invalid.rawValue
            objectMOC?.status = Int32(taskStatus)
            objectMOC?.taskType = Int32(taskType)
            if let tenantCanvas = task.tenantCanvas {
                objectMOC?.tenantCanvas = TenantCanvasMOC.insertInto(context: context, tenantCanvas: tenantCanvas)
            }
//            objectMOC?.tenantId = task.tenantId != nil && taskType == TaskType.tenantCanvas.rawValue ? Int32(task.tenantId!) : -1
            
            if let _notes = task.notes {
                let insertedNotes = TaskNotesMOC.insertInto(context: context, notes: _notes) as Any
                objectMOC?.taskNotes = NSSet(array: [insertedNotes])
//                objectMOC?.notes = NSSet(array: [insertedNotes])

            }
            
//            if let locations = task.locations {
//                for location in locations {
//                    let _location = LocationMOC.insertInto(context: context, location: location) as Any
//                    objectMOC?.locations = NSSet(array: [_location])
//                    //                objectMOC?.notes = NSSet(array: [insertedNotes])
//                }
//            }
        }
        return objectMOC
    }
}
