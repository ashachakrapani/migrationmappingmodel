//
//  TaskStatus.swift
//  MigrationPolicy
//
//  Created by Asha Chakrapani on 10/22/18.
//  Copyright © 2018 Asha Chakrapani. All rights reserved.
//

import Foundation

enum TaskStatus: Int {
    case invalid = 0
    case urgent = 1
    case notUrgent = 2
    case completed = 3
}
