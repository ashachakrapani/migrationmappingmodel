//
//  TaskType.swift
//  MigrationPolicy
//
//  Created by Asha Chakrapani on 10/22/18.
//  Copyright © 2018 Asha Chakrapani. All rights reserved.
//

import Foundation

enum TaskType: Int {
    case invalid = 0
    case tenantCanvas = 1
    case leadHunting = 2
}
