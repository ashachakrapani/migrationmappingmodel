//
//  Location.swift
//  MigrationPolicy
//
//  Created by Asha Chakrapani on 10/22/18.
//  Copyright © 2018 Asha Chakrapani. All rights reserved.
//

import Foundation

public struct Location {
    let locationId: Int?
    let locationName: String?
    let isActive: Bool?
}
