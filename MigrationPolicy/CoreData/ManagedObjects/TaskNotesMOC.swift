//
//  NotesMOC.swift
//  OneWithCustomCoreDataStack
//
//  Created by Asha Chakrapani on 9/11/18.
//  Copyright © 2018 Asha Chakrapani. All rights reserved.
//

import Foundation
import CoreData

@objc(TaskNotesMOC)
public class TaskNotesMOC: NSManagedObject {
    
    static func insertInto(context: NSManagedObjectContext, notes: String) -> TaskNotesMOC? {
        var objectMOC : TaskNotesMOC? = nil
        if let entity = NSEntityDescription.entity(forEntityName: "TaskNotes", in: context) {
            objectMOC = TaskNotesMOC(entity: entity, insertInto: context)
            objectMOC?.notes = notes
        }
        return objectMOC
    }
}
