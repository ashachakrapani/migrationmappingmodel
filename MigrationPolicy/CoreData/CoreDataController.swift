//
//  CoreDataController.swift
//  OneWithCustomCoreDataStack
//
//  Created by Asha Chakrapani on 9/11/18.
//  Copyright © 2018 Asha Chakrapani. All rights reserved.
//

import Foundation
import CoreData

class CoreDataController {
    
    func getAllTasks(onContext context: NSManagedObjectContext) -> [TaskMOC]? {
        let fetchRequest: NSFetchRequest<TaskMOC> = TaskMOC.fetchRequest()
        var tasks: [TaskMOC]? = nil
        do {
            tasks = try context.fetch(fetchRequest)
        } catch {
            print("Error fetching task data from CoreData: \(error.localizedDescription)")
        }
        return tasks
    }
    
    func getTaskCount(onContext context: NSManagedObjectContext) -> Int {
        let fetchRequest: NSFetchRequest<TaskMOC> = TaskMOC.fetchRequest()
        var tasks: [TaskMOC]? = nil
        do {
            tasks = try context.fetch(fetchRequest)
        } catch {
            print("Error fetching task data from CoreData: \(error.localizedDescription)")
        }
        return tasks?.count ?? 0
    }
    
    func updateLocation(forTaskId taskId: Int32, location: Location, onContext context: NSManagedObjectContext) {
        let fetchRequest: NSFetchRequest<TaskMOC> = TaskMOC.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "taskTitle = \"Task \(taskId)\"")
        
        do {
            if let _task = try context.fetch(fetchRequest).first, let locationId = location.locationId  {
                
                if let locationsMoc = _task.tenantCanvas?.locations?.allObjects as? [LocationMOC], locationsMoc.count > 0 {
//                if let locationsMoc = _task.locations?.allObjects as? [LocationMOC], locationsMoc.count > 0 {
                    if let locationMoc = (locationsMoc.filter({$0.locationId == Int32(locationId)})).first {
                        locationMoc.locationId = Int32(locationId)
                        locationMoc.locationName = location.locationName
                        locationMoc.isActive = location.isActive ?? false
                    }
                } else {
                    let locationArray = LocationMOC.insertInto(context: context, location: location) as Any
                    _task.tenantCanvas?.locations = NSSet(array: [locationArray])
//                    _task.locations = NSSet(array: [locationArray])
                }
                try context.save()
            }
        } catch {
            print("Error updating location for task \(taskId): \(error.localizedDescription)")
        }
    }
    
}
