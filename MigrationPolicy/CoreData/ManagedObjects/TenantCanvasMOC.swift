//
//  TenantCanvasMOC.swift
//  MigrationPolicy
//
//  Created by Asha Chakrapani on 10/23/18.
//  Copyright © 2018 Asha Chakrapani. All rights reserved.
//

import Foundation
import CoreData

@objc(TenantCanvasMOC)
public class TenantCanvasMOC: NSManagedObject {

    static func insertInto(context: NSManagedObjectContext, tenantCanvas: TenantCanvas) -> TenantCanvasMOC? {
        var objectMOC : TenantCanvasMOC? = nil
        if let entity = NSEntityDescription.entity(forEntityName: "TenantCanvas", in: context) {
            objectMOC = TenantCanvasMOC(entity: entity, insertInto: context)
            if let tenantId = tenantCanvas.tenantId {
                objectMOC?.tenantId = Int32(tenantId)
            }
            if let accessLevel = tenantCanvas.accessLevel {
                objectMOC?.accessLevel = Int32(accessLevel)
            }
            if let locations = tenantCanvas.locations {
                for location in locations {
                    let _location = LocationMOC.insertInto(context: context, location: location) as Any
                    objectMOC?.locations = NSSet(array: [_location])
                    //                objectMOC?.notes = NSSet(array: [insertedNotes])
                }
            }
        }
        return objectMOC
    }
}
