//
//  LocationMOC.swift
//  MigrationPolicy
//
//  Created by Asha Chakrapani on 10/22/18.
//  Copyright © 2018 Asha Chakrapani. All rights reserved.
//

import Foundation
import CoreData

@objc(LocationMOC)
public class LocationMOC: NSManagedObject {
    
    static func insertInto(context: NSManagedObjectContext, location: Location) -> LocationMOC? {
        var objectMOC : LocationMOC? = nil
        if let entity = NSEntityDescription.entity(forEntityName: "Location", in: context) {
            objectMOC = LocationMOC(entity: entity, insertInto: context)
            if let locationId = location.locationId {
                objectMOC?.locationId = Int32(locationId)
            }
            objectMOC?.locationName = location.locationName
            objectMOC?.isActive = location.isActive ?? false
        }
        return objectMOC
    }
}
