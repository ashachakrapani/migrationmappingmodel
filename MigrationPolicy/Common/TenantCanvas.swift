//
//  TenantCanvas.swift
//  MigrationPolicy
//
//  Created by Asha Chakrapani on 10/25/18.
//  Copyright © 2018 Asha Chakrapani. All rights reserved.
//

import Foundation

public struct TenantCanvas {
    let tenantId: Int?
    let accessLevel: Int?
    let locations: [Location]?
}
