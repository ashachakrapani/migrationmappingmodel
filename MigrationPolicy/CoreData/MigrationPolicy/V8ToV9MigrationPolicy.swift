//
//  TaskToTaskTypeMigrationV7ToV8.swift
//  MigrationPolicy
//
//  Created by Asha Chakrapani on 10/22/18.
//  Copyright © 2018 Asha Chakrapani. All rights reserved.
//

import CoreData

class V8ToV9MigrationPolicy: NSEntityMigrationPolicy {
    
    override func createDestinationInstances(forSource sInstance: NSManagedObject, in mapping: NSEntityMapping, manager: NSMigrationManager) throws {
        try super.createDestinationInstances(forSource: sInstance, in: mapping, manager: manager)
        
        let taskType = sInstance.value(forKey: "taskType") as! Int32
        let tenantId = sInstance.value(forKey: "tenantId") as! Int32
        if taskType == Int32(TaskType.tenantCanvas.rawValue) {
            if let tenantCanvasEntity = NSEntityDescription.entity(forEntityName: "TenantCanvas", in: manager.destinationContext) {
                let tenantCanvasInstance = NSManagedObject(entity: tenantCanvasEntity, insertInto: manager.destinationContext)
                tenantCanvasInstance.setValue(tenantId, forKey: "tenantId")
            }
        }
    }

    override func  createRelationships(forDestination dInstance: NSManagedObject, in mapping: NSEntityMapping, manager: NSMigrationManager) throws {
        try super.createRelationships(forDestination: dInstance, in: mapping, manager: manager)

        //This custom migrations aims at not having tenantId in the destination task model simply because it the not right place for the tenant id to be in. if the tenant id were left in task moc it
        //might make the below migration far less verbose.
        if let entityName = dInstance.entity.name, entityName == "Task", let taskTitle = dInstance.value(forKey: "taskTitle") as? String {
            //create relationship between Task and TenantCanvas
            //find the tenant id associated with this task from the source context so the relationship can be set with the corresponding tenant canvas instance in the destinationcontext
            if let sTenantIdForTask = self.findTenantIdInSourceContext(byTaskTitle: taskTitle, nsMigrationManager: manager),
                let dTenantCanvasInstance = self.findTenantCanvasDestinationInstance(forTenantId: sTenantIdForTask, nsMigrationManager: manager) {
                    dInstance.setValue(dTenantCanvasInstance, forKey: "tenantCanvas")
                
                //create relationship between tenant canvas and the location
                if let sLocationIds = self.findLocationIdsInSourceContext(byTaskTitle: taskTitle, nsMigrationManager: manager) {
                    //find the corresponding location ids in the destination to map relationships
                    print(sLocationIds)
                    var locationsMoc: [NSManagedObject] = []
                    for sLocationId in sLocationIds {
                        print(sLocationId)
                        if let dLocationInstance = self.getDestinationsInstance(forLocationId: sLocationId, nsMigrationManager: manager) {
                            locationsMoc.append(dLocationInstance)
//                            dLocationInstance.setValue(dTenantCanvasInstance, forKeyPath: "tenantCanvas")
                        }
                    }
                    dTenantCanvasInstance.setValue(NSSet(array: locationsMoc), forKeyPath: "locations")
                }
            }
         }
            
            //CANNOT/DO NOT use subclasses in migration mapping model. it will compile ok, but IT WILL NOT WORK at runtime!!!
//            let tenantCanvasMoc: TenantCanvasMOC?
//            let fetchRequest: NSFetchRequest<TenantCanvasMOC> = TenantCanvasMOC.fetchRequest()
//            print(tenantId)
//
//            fetchRequest.predicate = NSPredicate(format: "tenantId == \(tenantId)")
//            do {
//                tenantCanvasMoc = try manager.destinationContext.fetch(fetchRequest).first
//                dInstance.setValue(tenantCanvasMoc, forKey: "TenantCanvas")
//            } catch {
//                print("Error fetch task to create relationships \(error)")
//            }
//        }
    }
    
    //MARK: Private API
    private func findTenantIdInSourceContext(byTaskTitle taskTitle: String, nsMigrationManager manager: NSMigrationManager) -> Int32?  {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Task")
        fetchRequest.predicate = NSPredicate(format: "taskTitle == %@", taskTitle)
        fetchRequest.resultType = .managedObjectResultType
        do {
            let sourceTaskResults = try manager.sourceContext.fetch(fetchRequest)
            for sourceTaskEntity in sourceTaskResults {
                if let _sourceTaskEntity = (sourceTaskEntity as? NSManagedObject) {
                    let tenantIdFromSourceTask = _sourceTaskEntity.value(forKey: "tenantId") as! Int32
                    return tenantIdFromSourceTask
                }
            }
        } catch {
            print("Error fetch task to create relationships \(error)")
        }
        return nil
    }
    
    private func findTenantCanvasDestinationInstance(forTenantId tenantId: Int32, nsMigrationManager manager: NSMigrationManager) -> NSManagedObject? {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "TenantCanvas")
        fetchRequest.predicate = NSPredicate(format: "tenantId == \(tenantId)")
        fetchRequest.resultType = .managedObjectResultType
        do {
            let tenantCanvasResults = try manager.destinationContext.fetch(fetchRequest)
            for tenantCanvasEntity in tenantCanvasResults {
                return tenantCanvasEntity as? NSManagedObject
            }
        } catch {
            print("Error fetch task to create relationships \(error)")
        }
        return nil
    }
    
    private func findLocationIdsInSourceContext(byTaskTitle taskTitle: String, nsMigrationManager manager: NSMigrationManager) -> [Int32]? {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Task")
        fetchRequest.predicate = NSPredicate(format: "taskTitle == %@", taskTitle)
        fetchRequest.resultType = .managedObjectResultType
        do {
            let sourceTaskResults = try manager.sourceContext.fetch(fetchRequest)
            var locationsIds: [Int32] = []
            for sourceTaskEntity in sourceTaskResults {
                if let _sourceTaskEntity = (sourceTaskEntity as? NSManagedObject), let locationsFromSourceTask =  (_sourceTaskEntity.value(forKey: "locations") as? NSSet)?.allObjects as? [NSManagedObject] {
                    for location in locationsFromSourceTask {
                        if let locationId = location.value(forKey: "locationId") as? Int32 {
                            locationsIds.append(locationId)
                        }
                    }
                    return locationsIds
                }
            }
        } catch {
            print("Error fetch task to create relationships \(error)")
        }
        return nil
    }
    
    private func getDestinationsInstance(forLocationId locationId: Int32, nsMigrationManager manager: NSMigrationManager) -> NSManagedObject?  {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Location")
        fetchRequest.predicate = NSPredicate(format: "locationId == %d", locationId)
        fetchRequest.resultType = .managedObjectResultType
        do {
            let dLocationResults = try manager.destinationContext.fetch(fetchRequest)
            print(dLocationResults)
            //there should ideally be only one record for one location id
            for dLocationEntity in dLocationResults {
                print(dLocationEntity)
                if let _dLocationEntity = dLocationEntity as? NSManagedObject {
                    print(_dLocationEntity)
                    return _dLocationEntity
                }
            }
        } catch {
            print("Error fetch task to create relationships \(error)")
        }
        return nil
    }
}
