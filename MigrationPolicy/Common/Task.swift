//
//  Task.swift
//  MigrationPolicy
//
//  Created by Asha Chakrapani on 10/22/18.
//  Copyright © 2018 Asha Chakrapani. All rights reserved.
//

import Foundation

//public struct Task {
//    let taskTitle: String?
//    let taskDescription: String?
//    let taskType: TaskType?
//    let tenantId: Int?
//    let status: TaskStatus?
//    let notes: String?
//    let locations: [Location]?
//}

public struct Task {
    let taskTitle: String?
    let taskDescription: String?
    let taskType: TaskType?
    let status: TaskStatus?
    let notes: String?
    let tenantCanvas: TenantCanvas?
//    let leadHunting: LeadHunting?
}
