//
//  CoreDataStack.swift
//  OneWithCustomCoreDataStack
//
//  Created by Asha Chakrapani on 8/24/18.
//  Copyright © 2018 Asha Chakrapani. All rights reserved.
//


import Foundation
import CoreData

class CoreDataStack: NSObject {
    
    //MARK: - Singleton Implementation
    
    private override init() {
        super.init()
    }
    
    static var sharedInstance = CoreDataStack()
    
    //MARK: - Private API
    
    private var container = NSPersistentContainer(name: "Migration")
    
    
    //MARK: - Public API
    
    var mainContext: NSManagedObjectContext {
        get {
            return self.persistentContainer.viewContext
        }
    }
    
    lazy var persistentContainer: NSPersistentContainer = {
        
        let fileManager = FileManager.default
        let storeName = "Migration.sqlite"
        
        let documentsDirectoryURL = fileManager.urls(for: .documentDirectory, in: .userDomainMask)[0]
        let persistentStoreURL = documentsDirectoryURL.appendingPathComponent(storeName)
        print("Core data path : \(persistentStoreURL)" )
        
        let storeDescription = NSPersistentStoreDescription(url: persistentStoreURL)
//        storeDescription.shouldInferMappingModelAutomatically = true
        storeDescription.shouldMigrateStoreAutomatically = true
        
        self.container.persistentStoreDescriptions = [storeDescription]
        self.container.loadPersistentStores(completionHandler: { (persistentStoreDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        
        return self.container
    }()
    
    func saveMainContext() {
        let context = self.persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                //Fatal error here, because no store - no dice.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
}

